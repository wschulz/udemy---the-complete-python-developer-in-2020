#Fundamental Data Types

#int and float  (Integers and Floting Point Numbers)
print(type(2 + 4))
print(type(2 - 4))
print(type(2 * 4))
print(type(2 / 4)) #0.5

print(2 ** 3)
print(11 // 4)
print(5 % 4)

#math functions
print(round(3.1))
print(abs(-20))

# operator precedence
print((20 - 3) + 2 ** 2)

# ()    1. Klammern
# **    2. Hochgestellt
# */    3. Punktrechung
# +-    4. Strichrechung

# complex (another data type, only to know it exist)

# augmented assignment operator
some_value = 5                  # 5
some_value = 5 + 2              # 7
some_value = some_value + 2     # 9
some_value += 2                 #11

print(some_value)

# bool

# str   (String)
print(type("hi, hello there!"))
username = 'supercoder'
password = 'supersecret'
long_string = '''
Wow, thisis
really nice
i can do line
shifts!
'''
print(long_string)

first_name = "Wolfgang"
last_name ="Schulz"
full_name = first_name + " " + last_name
print(full_name)

# string concatenation
print("Hello" + " Andrei")

# type conversion
print(type(int(str(100))))
# the same as
a = 100
b = str(a)
c = int(b)
print(c)

# Escape sequence
weather = "\tIt's \"kind of\" sunny\nhope you have a good day"
print(weather)

# formatted strings
name = 'Johnny'
age = 45

print('Hi ' + name + '. You are ' + str(45) + ' years old.')
print('Hi {}. You are {} years old.'.format(name, age))
print('Hi {1}. You are {0} years old.'.format(age, name))
print(f'Hi {name}. You are {age} years old.')

# string manipulation

selfish = 'me me me'
        #  01234567
# slicing
# [start:stop:stepover]
print(selfish[0])
print(selfish[0:7])
print(selfish[0:8:2])

#immutability


# list
# tuple
# set
# dict

# #Classes -> custom types

# #Specialized Data Types

# None
